ago
alembic
amqpy
argon2_cffi
black
bleach
click
cornice
freezegun
gunicorn
html5lib
ipython
mypy
mypy-extensions
Pillow
prometheus-client
prospector
psycopg2
publicsuffix2==2.20160818
Pygments
pyotp
pyramid
pyramid-debugtoolbar
pyramid-ipython
pyramid-jinja2
pyramid-session-redis
pyramid-tm
pyramid-webassets
pytest
pytest-mock
PyYAML  # needs to be installed separately for webassets
qrcode
sentry-sdk
SQLAlchemy
SQLAlchemy-Utils
stripe
testing.redis
titlecase
webargs<5.0  # 5.0.0 breaks many views, will require significant updates
webtest
wrapt
zope.sqlalchemy
